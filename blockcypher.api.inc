<?php
/**
 * @file
 * BlockCypher API functions.
 *
 * Various helper API functions, so that users of the module don't need to
 * interface with the SDK directly (unless they absolutely want to).
 */

// We use all of these namespaces to simplify the code full of SDK calls.
use BlockCypher\Auth\SimpleTokenCredential;

use BlockCypher\Api\AddressList;
use BlockCypher\Api\TXInput;
use BlockCypher\Api\TXOutput;
use BlockCypher\Api\TX;
use BlockCypher\Api\Wallet;
use BlockCypher\Api\WebHook;

use BlockCypher\Client\AddressClient;
use BlockCypher\Client\BlockClient;
use BlockCypher\Client\BlockchainClient;
use BlockCypher\Client\FaucetClient;
use BlockCypher\Client\MicroTXClient;
use BlockCypher\Client\PaymentForwardClient;
use BlockCypher\Client\TXClient;
use BlockCypher\Client\WalletClient;
use BlockCypher\Client\WebHookClient;

use BlockCypher\Rest\ApiContext;

use BlockCypher\Validation\TokenValidator;

/**
 * Authentication functions.
 *
 * These are needed to connect to the API service.
 */

/**
 * Creates and returns a BlockCypher ApiContext object.
 *
 * This is needed to make any calls to the API using their php SDK. You probably
 * don't want to call this one directly. Instead, call blockcypher_initialize()
 * which in turn calls this function.
 *
 * @param array $params
 *   An optional array of parameters to override the default API connection
 *   settings. Valid options are:
 *   - token: Use a different API token than the default.
 *   - api_version: Which API version to use.
 *   - coin: The cryptocoin we are working with: (btc|ltc|doge|bcy)
 *   - chain: The blockchain of the specified coin we want to connect to.
 *       (main|test|test3)
 *   - log_enabled: If set to TRUE, then the BlockCypher SDK will log its own
 *       messages to a file.
 *   - log_filename: If logging is enabled, this is the path to the file that
 *       will be written to.
 *   - log_level: The level of logging. Valid options are 'INFO' and 'DEBUG'.
 *
 * @return object
 *   A BlockCypher APIContext object, needed for all of our API calls.
 */
function blockcypher_create_api_context($params = array()) {
  $api_context = NULL;

  $token = isset($params['token']) ? $params['token'] : variable_get('blockcypher_api_token', '');
  $api_version = isset($params['api_version']) ? $params['api_version'] : variable_get('blockcypher_default_api_version', BLOCKCYPHER_DEFAULT_API_VERSION);

  $default_blockchain = variable_get('blockcypher_default_blockchain', BLOCKCYPHER_BLOCKCHAIN_BCYTEST);
  $default_blockchain = explode('.', $default_blockchain);

  $config = array(
    'log.LogEnabled' => isset($params['log_enabled']) ? $params['log_enabled'] : variable_get('blockcypher_logging_enabled', 0),
    'log.FileName' => isset($params['log_filename']) ? $params['log_filename'] : variable_get('blockcypher_log_filepath', BLOCKCYPHER_DEFAULT_LOG_FILEPATH),
    'log.LogLevel' => isset($params['log_level']) ? $params['log_level'] : variable_get('blockcypher_log_level', BLOCKCYPHER_LOG_LEVEL_INFO),
    'validation.level' => 'log',
  );

  if (_blockcypher_token_is_valid($token)) {
    $api_context = ApiContext::create(
      isset($params['chain']) ? $params['chain'] : $default_blockchain[1],
      isset($params['coin']) ? $params['coin'] : $default_blockchain[0],
      $api_version,
      new SimpleTokenCredential($token),
      $config
    );
  }
  else {
    $link_args = array('attributes' => array('target' => '_blank'));
    $replacements = array(
      '!link' => l(t('https://accounts.blockcypher.com'), 'https://accounts.blockcypher.com', $link_args),
    );
    $message = 'Invalid BlockCypher API token. Generate a new one: !link';
    watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
  }

  return $api_context;
}

/**
 * Validate a BlockCypher Token.
 *
 * @param string $token
 *   A BlockCypher API token string.
 *
 * @return bool
 *   TRUE if the given token is valid, FALSE otherwise.
 */
function _blockcypher_token_is_valid($token) {
  return TokenValidator::validate($token);
}

/**
 * Address Functions.
 *
 * Create & load addresses. Both normal & multisig.
 */

/**
 * For a given address, return an array of information about it.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $address
 *   An address on the blockchain.
 *
 * @return array
 *   An array containing all of the information about the requested address.
 */
function blockcypher_get_address($api_context, $address) {
  $address_client = new AddressClient($api_context);
  $address_array = array();
  try {
    $address_object = $address_client->get($address);
    $address_array = $address_object->toArray();
  }
  catch (Exception $error) {
    $replacements = array(
      '%address' => $address,
      '%error' => $error->getMessage(),
    );
    $message = 'Error trying to fetch details for address "%address":<br/><br/>%error';
    watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
  }

  return $address_array;
}

/**
 * Generate a new address & private key.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 *
 * @return array
 *   An array containing all of the information about the newly-created address.
 */
function blockcypher_create_address($api_context) {
  $address_client = new AddressClient($api_context);
  $address_key_chain_array = array();
  try {
    $address_key_chain = $address_client->generateAddress();
    $address_key_chain_array = $address_key_chain->toArray();
  }
  catch (Exception $error) {
    $replacements = array('%error' => $error->getMessage());
    $message = 'Error trying to generate a new address:<br/><br/>%error';
    watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
  }

  return $address_key_chain_array;
}

/**
 * Using multiple public keys, generate an n-of-m multisig address.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string[] $public_keys
 *   An array of public keys.
 * @param int $n
 *   The minimum number of signatures required to sign a transaction.
 *
 * @return array
 *   An array containing all of the information about the newly-created
 *   multisig address.
 */
function blockcypher_create_multisig_address($api_context, $public_keys, $n) {
  $multisig_address_array = array();

  $address_client = new AddressClient($api_context);
  try {
    $multisig_address = $address_client->generateMultisigAddress($public_keys, 'multisig-' . $n . '-of-' . count($public_keys));
    $multisig_address_array = $multisig_address->toArray();
  }
  catch (Exception $error) {
    $replacements = array('%error' => $error->getMessage());
    $message = 'Error trying to generate a new address:<br/><br/>%error';
    watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
  }

  return $multisig_address_array;
}

/**
 * Block Functions.
 *
 * Look up information about specific blocks on the blockchain.
 */

/**
 * Fetch information about a specific block.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param int|string $id
 *   If numeric, The block is fetched by height. If alphanumeric, the block is
 *   fetched by hash.
 *
 * @return array
 *   An array containing all of the information about the requested block.
 */
function blockcypher_get_block($api_context, $id) {
  $block_array = array();
  $block_client = new BlockClient($api_context);
  try {
    $block = $block_client->get($id);
    $block_array = $block->toArray();
  }
  catch (Exception $error) {
    $replacements = array('%id' => $id, '%error' => $error->getMessage());
    $message = 'Error trying to fetch details for block with height/hash "%id":<br/><br/>%error';
    watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
  }

  return $block_array;
}

/**
 * Return an array of information about multiple requested blocks.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param int[]|string[] $id_array
 *   $ids An array of block ids (either the hash or the height).
 *
 * @return array
 *   An array of arrays (keyed by height), each sub-array containing information
 *   about a requested block.
 *
 * @see blockcypher_get_block()
 */
function blockcypher_get_blocks($api_context, $id_array) {
  $block_array = array();
  $block_client = new BlockClient($api_context);
  try {
    $blocks = $block_client->getMultiple($id_array);
    foreach ($blocks as $block) {
      $block_array[$block->getHeight()] = $block->toArray();
    }
  }
  catch (Exception $error) {
    $replacements = array(
      '%ids' => implode(', ', $id_array),
      '%error' => $error->getMessage(),
    );
    $message = 'Error trying to fetch details for block with height/hashes: %ids<br/><br/>%error';
    watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
  }

  return $block_array;
}

/**
 * Blockchain Functions.
 *
 * Call up info about a specific blockchain.
 */

/**
 * Return an array of top-level information about an entire blockchain.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 *
 * @return array
 *   An array containing information about the blockchain specified in
 *   the $api_context.
 */
function blockcypher_get_blockchain($api_context) {
  $blockchain_client = new BlockchainClient($api_context);
  $blockchain_array = array();
  $name = '';
  try {
    $name = $api_context->getCoin() . '.' . $api_context->getChain();
    $blockchain = $blockchain_client->get($name);
    $blockchain_array = $blockchain->toArray();
  }
  catch (Exception $error) {
    $replacements = array('%name' => $name, '%error' => $error->getMessage());
    $message = 'Error trying to fetch details for blockchain "%name":<br/><br/>%error';
    watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
  }

  return $blockchain_array;
}

/**
 * Wallet Functions.
 *
 * It's worth pointing out that "Wallet" has nothing to do with private key
 * management in Blockcypher.
 *
 * It's more like just a named collection of addresses.  For example, you can
 * create a wallet for each username on your site and then easily pull up a
 * given user's address list by name. You will still need to store the private
 * keys on your own or they will be lost forever.
 */

/**
 * Create a new (non-hd) wallet.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $name
 *   The name of the wallet. Note: these names are unique to the blockchain
 *   they're on. This means that you cannot have a wallet and an HD wallet with
 *   the same name on the same blockchain.
 * @param string[] $addresses
 *   An optional array of addresses to add to this wallet upon creation.
 *
 * @return array
 *   An array containing information on the newly-created wallet.
 */
function blockcypher_create_wallet($api_context, $name, $addresses = array()) {
  $wallet_output = array();

  $wallet = new Wallet();
  $wallet->setName($name);
  if (!empty($addresses)) {
    $wallet->setAddresses($addresses);
  }

  $wallet_client = new WalletClient($api_context);
  try {
    $wallet = $wallet_client->create($wallet);
    $wallet_output = $wallet->toArray();
  }
  catch (Exception $error) {
    $replacements = array(
      '%name' => $name,
      '%addr' => implode(', ', $addresses),
      '%error' => $error->getMessage(),
    );
    $message = 'Error trying to create wallet with name "%name" and addresses "%addr":<br/><br/>%error';
    watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
  }

  return $wallet_output;
}


/**
 * Get an existing (non-hd) wallet.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $name
 *   The name of the wallet.
 *
 * @return array
 *   An array containing information on the requested wallet.
 */
function blockcypher_get_wallet($api_context, $name) {
  $wallet_output = array();

  $wallet_client = new WalletClient($api_context);
  try {
    $wallet = $wallet_client->get($name);
    $wallet_output = $wallet->toArray();
  }
  catch (Exception $error) {
    $replacements = array('%name' => $name, '%error' => $error->getMessage());
    $message = 'Error trying to get wallet with name "%name":<br/><br/>%error';
    watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
  }

  return $wallet_output;
}

/**
 * Add one or more addresses to an existing wallet.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $name
 *   The name of the wallet.
 * @param string[] $addresses
 *   An array of addreses to add to the wallet.
 *
 * @return array
 *   An array with information about the newly-updated wallet.
 */
function blockcypher_add_addresses_to_wallet($api_context, $name, $addresses) {
  $wallet_output = array();

  $wallet_client = new WalletClient($api_context);
  $address_list = AddressList::fromAddressesArray($addresses);
  try {
    $wallet = $wallet_client->addAddresses($name, $address_list);
    $wallet_output = $wallet->toArray();
  }
  catch (Exception $error) {
    $replacements = array(
      '%name' => $name,
      '%addr' => implode(', ', $addresses),
      '%error' => $error->getMessage(),
    );
    $message = 'Error trying to add addresses "%addr" to the wallet with name "%name":<br/><br/>%error';
    watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
  }

  return $wallet_output;
}

/**
 * Remove one or more addresses from an existing wallet.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $name
 *   The name of the wallet.
 * @param string[] $addresses
 *   An array of addresses to remove from this wallet.
 *
 * @return bool
 *   TRUE if the removal succeeded, FALSE otherwise.
 *
 * @todo: I would prefer if this returned the modified wallet, but I think there
 *   is a bug in the sdk.  toArray() is not working on the object returned from
 *   wallet client.
 */
function blockcypher_remove_addresses_from_wallet($api_context, $name, $addresses) {
  $success = FALSE;

  $wallet_client = new WalletClient($api_context);
  $address_list = AddressList::fromAddressesArray($addresses);
  try {
    $wallet_client->removeAddresses($name, $address_list);
    $success = TRUE;
  }
  catch (Exception $error) {
    $replacements = array(
      '%name' => $name,
      '%addr' => implode(', ', $addresses),
      '%error' => $error->getMessage(),
    );
    $message = 'Error trying to remove addresses "%addr" from the wallet with name "%name":<br/><br/>%error';
    watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
  }

  return $success;
}

/**
 * Delete an existing wallet.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $name
 *   The name of the wallet.
 *
 * @return bool
 *   TRUE if the wallet was successfully deleted, FALSE if there was an error.
 */
function blockcypher_delete_wallet($api_context, $name) {
  $success = FALSE;

  $wallet_client = new WalletClient($api_context);
  try {
    $success = $wallet_client->delete($name);
  }
  catch (Exception $error) {
    $replacements = array('%name' => $name, '%error' => $error->getMessage());
    $message = 'Error trying to delete wallet with name "%name":<br/><br/>%error';
    watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
  }

  return $success;
}

/**
 * Transaction functions.
 *
 * Load info about existing transactions, or create new ones and push them
 * to the network.
 *
 * Remember: once a transaction is on the network it cannot be reversed!!!
 */

/**
 * Fund an address from a faucet.
 *
 * If you try to do this on any blockchain other than BTC/Testnet3 or
 * BCY/Testnet then it will fail.
 *
 * On the BlockCypher Test Chain, the faucet will refuse to fund an address
 * with more than 500 billion BlockCypher satoshis and will not fund more than
 * 100 million BlockCypher satoshis at a time. On Bitcoin Testnet3 those numbers
 * are adjusted to 10 million and 500,000 testnet satoshis respectively.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $address
 *   The address to fund.
 * @param int $amount
 *   The amount (in satoshi) to send to this address.
 *
 * @return array
 *   An array containing the transaction id of the faucet funding the address.
 */
function blockcypher_fund_address_from_faucet($api_context, $address, $amount) {
  $faucet_response_array = array();
  $faucet_client = new FaucetClient($api_context);
  try {
    $faucet_response = $faucet_client->fundAddress($address, $amount);
    $faucet_response_array = $faucet_response->toArray();
  }
  catch (Exception $error) {
    $replacements = array(
      '%addr' => $address,
      '%amount' => $amount,
      '%error' => $error->getMessage(),
    );
    $message = 'Error trying to fund the address "%addr" with %amount satoshi from a faucet:<br/><br/>%error';
    watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
  }

  return $faucet_response_array;
}

/**
 * Fetch and return information for a given transaction on the blockchain.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $tx_hash
 *   The transaction hash.
 *
 * @return array
 *   An array containing information on the requested transaction.
 */
function blockcypher_get_transaction($api_context, $tx_hash) {
  $tx_array = array();
  $tx_client = new TXClient($api_context);
  try {
    $transaction = $tx_client->get($tx_hash);
    $tx_array = $transaction->toArray();
  }
  catch (Exception $error) {
    $replacements = array(
      '%hash' => $tx_hash,
      '%error' => $error->getMessage(),
    );
    $message = 'Error trying to fetch the transaction with the hash "%hash":<br/><br/>%error';
    watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
  }

  return $tx_array;
}

/**
 * Create a transaction, sign it, and then push it to the network.
 *
 * This function lets you send funds from one addresses to either a standard
 * or multisig address.
 *
 * If the transaction is small enough, it is sent as a blockcypher
 * microtransaction, which seems to save some fees.
 *
 * DANGER: Once a transaction is pushed to the network, it cannot be reversed!
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $src
 *   The address that you are sending from.
 * @param string|string[] $dest
 *   The address you are sending the coins to (or an array of public keys if
 *   the destination is a multisig address).
 * @param string $private
 *   The private key of the sending address, used to sign the transaction.
 * @param int $amount
 *   The amount (in satoshi) to send from the source address to the
 *   destination address.
 * @param string $dest_script_type
 *   If the receiving address is multisig, this needs to be a string in the
 *   format of 'multisig-n-of-m'.
 *
 * @todo: allow the user to specify a change address, instead of automatically
 *   refunding to the source address.
 * @todo: allow the user to spend from a multisig address
 *   (or do this in its own function?).
 * @todo: allow the user to specify the miner fee rather than letting the
 *   api decide on its own.
 *
 * @return array
 *   An array containing the information about the newly-created unconfirmed transaction.
 */
function blockcypher_create_and_send_transaction($api_context, $src, $dest, $private, $amount, $dest_script_type = 'pay-to-pubkey-hash') {
  $output = NULL;

  $src = is_array($src) ? $src : array($src);
  $dest = is_array($dest) ? $dest : array($dest);
  $private = is_array($private) ? $private : array($private);

  // Configure transaction inputs.
  $input = new TXInput();
  $input->setAddresses($src);

  // Configure transaction outputs.
  $output = new TXOutput();
  $output->setAddresses($dest);
  $output->setScriptType($dest_script_type);

  $output->setValue($amount);

  $tx = new TX();
  $tx->addInput($input);
  $tx->addOutput($output);

  // If this is a simple transaction with an amount less then the
  // microtransaction API limit (4,000,000 at the time of this writing),
  // then we go ahead and send it as a microtransaction instead.
  if (count($dest) == 1 && $amount < BLOCKCYPHER_MICROTX_MAX_AMOUNT) {
    $micro_tx_client = new MicroTXClient($api_context);
    try {
      $tx = $micro_tx_client->sendSigned(array_values($private)[0], array_values($dest)[0], $amount);
      $output = $tx->toArray();
    }
    catch (Exception $error) {
      $replacements = array(
        '%amt' => $amount,
        '%sender' => implode(', ', $src),
        '%dest' => implode(', ', $dest),
        '%error' => $error->getMessage(),
      );
      $message = 'Error trying to sign and send a microtransaction (%amt satoshi from sender(s): "%sender" to destination: "%dest"):<br/><br/>%error';
      watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
    }
  }
  else {
    // This requires a fully-fledged transaction.
    $tx_client = new TXClient($api_context);
    try {
      $tx = $tx_client->create($tx);
      $tx = $tx_client->sign($tx, $private);
      $tx = $tx_client->send($tx);
      $output = $tx->toArray();
    }
    catch (Exception $error) {
      $replacements = array(
        '%amt' => $amount,
        '%sender' => implode(', ', $src),
        '%dest' => implode(', ', $dest),
        '%error' => $error->getMessage(),
      );
      $message = 'Error trying to sign and send a transaction (%amt satoshi from sender(s): "%sender" to destination: "%dest"):<br/><br/>%error';
      watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
    }
  }

  return $output;
}

/**
 * Return the BlockCypher confidence rating for an unconfirmed transaction.
 *
 * This is defined as the probability that it will be included in the
 * next block. If this is called on a confirmed transaction, the API will return
 * an error.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $tx_hash
 *   The hash of the unconfirmed transaction.
 *
 * @return array
 *   An array with BlockCypher's confidence rating for the transaction,
 *   as a float from 0 to 1.
 */
function blockcypher_get_transaction_confidence($api_context, $tx_hash) {
  $confidence = array();

  $tx_client = new TXClient($api_context);
  try {
    $confidence = $tx_client->getConfidence($tx_hash);
    $confidence = $confidence->toArray();
  }
  catch (Exception $error) {
    $replacements = array(
      '%hash' => $tx_hash,
      '%error' => $error->getMessage(),
    );
    $message = 'Error trying to fetch the confidence rating for a transaction with the hash "%hash":<br/><br/>%error';
    watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
  }

  return $confidence;
}

/**
 * Payment-forwarding functions.
 *
 * Want to be able to give each user an address and then immediately sweep
 * deposits to your hot-wallet / cold storage? Want to have a webhook
 * automatically created so that your site gets pinged automatically with the
 * relevant information when a payment is made? Then these functions are the
 * greatest thing ever.
 */

/**
 * Create a new forwarding address.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $dest_address
 *   The destination address. When funds are sent to this forwarding address,
 *   they are immediately forwarded to here.
 * @param string $callback_url
 *   This is the url that will get hit with the payload whenever funds are
 *   automatically forwarded. If not provided, it will use the default
 *   payment-forwarded webhook url provided by this module. You can then later
 *   react to this webhook by implementing hook_blockcypher_payment_forwarded().
 * @param array $callback_url_params
 *   An array of params to be combined with the $callback_url using Drupal's
 *   url() function.
 *
 * @return array
 *   An array containing all of the information on the newly-created
 *   forwarding address.
 */
function blockcypher_create_forwarding_address($api_context, $dest_address, $callback_url = NULL, $callback_url_params = array()) {
  $forwarding_address = array();

  $callback_url = $callback_url ? $callback_url : BLOCKCYPHER_WEBHOOK_DEFAULT_FORWARDING_ADDRESS_URL;
  $callback_url_params['absolute'] = TRUE;
  $callback_url_params['https'] = TRUE;
  $callback_url_params['query']['secret'] = variable_get('blockcypher_webhook_secret_key', '');
  $options = array('callback_url' => url($callback_url, $callback_url_params));

  $payment_forward_client = new PaymentForwardClient($api_context);
  try {
    $forwarding_address = $payment_forward_client->createForwardingAddress($dest_address, $options);
    $forwarding_address = $forwarding_address->toArray();
  }
  catch (Exception $error) {
    $replacements = array(
      '%addr' => $dest_address,
      '%url' => isset($options['callback_url']) ? $options['callback_url'] : '',
      '%error' => $error->getMessage(),
    );
    $message = 'Error trying to create a forwarding address with destination address "%addr" and callback url "%url":<br/><br/>%error';
    watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
  }

  return $forwarding_address;
}

/**
 * Return the information about a specific forwarding address for a given ID.
 *
 * This ID isn't an address - it's the 'id' component assigned automatically
 * when you created the forwarding address.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $id
 *   The ID of the forwarding address.
 *
 * @return array
 *   An array of information about the existing forwarding address.
 */
function blockcypher_get_forwarding_address($api_context, $id) {
  $forwarding_address = array();

  $payment_forward_client = new PaymentForwardClient($api_context);
  try {
    $forwarding_address = $payment_forward_client->getForwardingAddress($id);
    $forwarding_address = $forwarding_address->toArray();
  }
  catch (Exception $error) {
    $replacements = array(
      '%id' => $id,
      '%error' => $error->getMessage(),
    );
    $message = 'Error trying to fetch an existing forwarding address with the id "%id":<br/><br/>%error';
    watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
  }

  return $forwarding_address;
}

/**
 * Fetch the complete list of forwarding addresses created for your API token.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 *
 * @return array
 *   An array of arrays, each describing an existing forwarding address.
 */
function blockcypher_get_forwarding_address_list($api_context) {
  $forwarding_addresses = array();

  $payment_forward_client = new PaymentForwardClient($api_context);
  try {
    $forwarding_addresses_result = $payment_forward_client->listForwardingAddresses();
    foreach ($forwarding_addresses_result as $result) {
      $forwarding_addresses[] = $result->toArray();
    }
  }
  catch (Exception $error) {
    $replacements = array(
      '%error' => $error->getMessage(),
    );
    $message = 'Error trying to fetch the complete list of forwarding addresses:<br/><br/>%error';
    watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
  }

  return $forwarding_addresses;
}

/**
 * Delete a payment forwarding address by its given ID.
 *
 * This ID isn't an address - it's the 'id' component assigned automatically
 * when you created the forwarding address.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $id
 *   The ID of the forwarding address.
 *
 * @return bool
 *   Return TRUE if deleted successfully, FALSE if there was an error.
 */
function blockcypher_delete_forwarding_address($api_context, $id) {
  $success = FALSE;

  $payment_forward_client = new PaymentForwardClient($api_context);
  try {
    $success = $payment_forward_client->deleteForwardingAddress($id);
  }
  catch (Exception $error) {
    $replacements = array(
      '%id' => $id,
      '%error' => $error->getMessage(),
    );
    $message = 'Error trying to delete an existing forwarding address with the id "%id":<br/><br/>%error';
    watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
  }

  return $success;
}

/**
 * Webhooks.
 *
 * The BlockCypher API allows you setup webhooks such that anytime a specified
 * event that you want to be notified about occurs (like your transaction was
 * confirmed, a new block was mined, a double-spend was detected, etc) then
 * BlockCypher will deliver a JSON payload to the site detailing it. You can
 * then react to this in your own module by implementing
 * hook_blockcypher_webhook_event().
 */

/**
 * Create a new Webhook.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $event_type
 *   This determines what will cause this webhook to trigger. Valid options are:
 *   - unconfirmed-tx: Triggered for every new transaction BlockCypher receives
 *       before it’s confirmed in a block; basically, for every unconfirmed
 *       transaction. The payload is an unconfirmed TX.  Warning:
 *       if unconstrained, this webhook will spam your $callback_url
 *   - new-block: Triggered for every new block added to the blockchain you’ve
 *       selected as your base resource. The payload is a Block.
 *   - confirmed-tx: Triggered for every new transaction making it into a new
 *       block; in other words, for every first transaction confirmation. This
 *       is equivalent to listening to the new-block event and fetching each
 *       transaction in the new Block. The payload is a confirmed TX.
 *   - tx-confirmation: Simplifies listening to confirmations on all
 *       transactions for a given address up to a provided threshold. Sends
 *       first the unconfirmed transaction and then the transaction for each
 *       confirmation. Use the confirmations property within the Event to
 *       manually specify the number of confirmations desired (maximum 10,
 *       defaults to 6). The payload is a TX.
 *   - double-spend-tx: Triggered any time a double spend is detected by
 *       BlockCypher. The payload is the TX that triggered the event; the hash
 *       of the transaction that it’s trying to double spend is included in its
 *       double_spend_tx property.
 *   - tx-confidence: Triggered any time an address has an unconfirmed
 *       transaction above the confidence property specified in the Event, based
 *       on our Confidence Factor. The payload is the TX that triggered the
 *       event. If confidence is not set, defaults to 0.99. To ensure
 *       transactions are not missed, even if your confidence threshold is not
 *       reached, a transaction is still sent after a minute timeout; please
 *       remember to double-check the confidence attribute in the TX payload.
 * @param string $hash
 *   Only objects matching this hash will be sent. The hash can be either for
 *   a block or a transaction.
 * @param string $address
 *   Only transactions associated with this address will be sent. This can
 *   either be an address OR the name of a wallet!
 * @param string $callback_url
 *   This is the url that will get hit with the payload when the webhook fires.
 *   If not provided, it will use the default webhook url provided by this
 *   module. You can then later react to this webhook by implementing
 *   hook_blockcypher_webhook_event().
 * @param array $callback_url_params
 *   An array of params to be combined with the $callback_url using
 *   Drupal's url() function.
 *
 * @return array
 *   An array describing the newly-created webhook.
 */
function blockcypher_create_webhook($api_context, $event_type, $hash = NULL, $address = NULL, $callback_url = NULL, $callback_url_params = array()) {
  $output = array();
  $webhook = new WebHook();

  $callback_url = $callback_url ? $callback_url : BLOCKCYPHER_WEBHOOK_DEFAULT_URL;
  $callback_url_params['absolute'] = TRUE;
  $callback_url_params['https'] = TRUE;
  $callback_url_params['query']['secret'] = variable_get('blockcypher_webhook_secret_key', '');
  $callback_full = url($callback_url, $callback_url_params);

  $webhook->setUrl($callback_full);
  $webhook->setEvent($event_type);

  if ($hash) {
    $webhook->setHash($hash);
  }

  if ($address) {
    $webhook->setAddress($address);
  }

  $webhook_client = new WebHookClient($api_context);
  try {
    $output = $webhook_client->create($webhook);
    $output = $output->toArray();
  }
  catch (Exception $error) {
    $replacements = array(
      '%etype' => $event_type,
      '%hash' => $hash,
      '%addr' => $address,
      '%cb' => $callback_full,
      '%error' => $error->getMessage(),
    );
    $message = 'Error trying to create a new webhook with the event_type "%etype", hash "%hash", address "%addr", callback url "%cb":<br/><br/>%error';
    watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
  }

  return $output;
}

/**
 * Return all of the info on an existing webhook, by id.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $id
 *   The id of the webhook.
 *
 * @return array
 *   An array describing the existing webhook.
 */
function blockcypher_get_webhook($api_context, $id) {
  $webhook = array();

  $webhook_client = new WebHookClient($api_context);
  try {
    $webhook_object = $webhook_client->get($id);
    $webhook = $webhook_object->toArray();
  }
  catch (Exception $error) {
    $replacements = array(
      '%error' => $error->getMessage(),
      '%id' => $id,
    );
    $message = 'Error trying to fetch the webhook with the id "%id":<br/><br/>%error';
    watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
  }

  return $webhook;
}

/**
 * Return an array of all webhooks created for this api token.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 *
 * @return array
 *   An array of arrays, each containing information on an existing webhook.
 */
function blockcypher_get_webhook_list($api_context) {
  $webhooks = array();

  $webhook_client = new WebHookClient($api_context);
  try {
    $webhook_objects = $webhook_client->getAll();
    foreach ($webhook_objects as $result) {
      $webhooks[] = $result->toArray();
    }
  }
  catch (Exception $error) {
    $replacements = array(
      '%error' => $error->getMessage(),
    );
    $message = 'Error trying to fetch the complete list of webhooks:<br/><br/>%error';
    watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
  }

  return $webhooks;
}

/**
 * Delete an existing webhook, by id.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $id
 *   The ID of the existing webhook.
 *
 * @return bool
 *   TRUE if the webhook was deleted successfully, FALSE if there was an error.
 */
function blockcypher_delete_webhook($api_context, $id) {
  $success = FALSE;

  $webhook_client = new WebHookClient($api_context);
  try {
    $success = $webhook_client->delete($id);
  }
  catch (Exception $error) {
    $replacements = array(
      '%error' => $error->getMessage(),
      '%id' => $id,
    );
    $message = 'Error trying to fetch the webhook with the id "%id":<br/><br/>%error';
    watchdog('blockcypher', $message, $replacements, WATCHDOG_ERROR);
  }

  return $success;
}
