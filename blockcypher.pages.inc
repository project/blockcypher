<?php
/**
 * @file
 * Page callbacks for the BlockCypher module.
 */

/**
 * Page callback for the custom webhook endpoint.
 *
 * If BlockCypher is telling us a payment was forwarded, then we pass the
 * payload over to all modules that want to do something with it.
 */
function blockcypher_webhook_endpoint() {
  $data = _blockcypher_process_post_request();
  if (!empty($data)) {
    module_invoke_all('blockcypher_webhook_event', $data);
  }
}

/**
 * Page callback for the payment-forwarding webhook endpoint.
 *
 * If BlockCypher is telling us that one of our custom webhooks
 * was triggered, we pass the payload over to all modules that want to
 * do something with it.
 */
function blockcypher_webhook_forwarding_address_endpoint() {
  $data = _blockcypher_process_post_request();
  if (!empty($data)) {
    module_invoke_all('blockcypher_payment_forwarded', $data);
  }
}

/**
 * Receive and validate a payload delivered to a webhook endpoint.
 *
 * Verify that the request is a POST and that it contains our secret webhook
 * key. If all is good, then we convert the data into an array and return it to
 * the page handler.
 *
 * @see http://www.drupalconnect.com/blog/articles/programming-restful-endpoints-accept-post-drupal
 *
 * @return array
 *   An array of the json-decoded data. This will be empty if the request
 *   was bad or failed our security check.
 */
function _blockcypher_process_post_request() {
  $data = array();

  if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    header('HTTP/1.1 405 Not Post');
    return $data;
  }

  if ($secret = variable_get('blockcypher_webhook_secret_key', '')) {
    $submitted_secret = isset($_GET['secret']) ? check_plain($_GET['secret']) : '';
    if ($secret != $submitted_secret) {
      header('HTTP/1.1 204 Incorrect Data');
      return $data;
    }
  }

  $received_json = file_get_contents("php://input", TRUE);
  $data = drupal_json_decode($received_json, TRUE);
  header('HTTP/1.1 201 Created');

  return $data;
}
