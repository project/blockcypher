<?php
/**
 * @file
 * BlockCypher main module file.
 *
 * The BlockCypher module allows you, a developer, to integrate the BlockCypher
 * API into your Drupal project, so that you can quickly and easily create and
 * read data on various blockchain networks.
 *
 * @see http://dev.blockcypher.com/#introduction
 */

// This is currently the only API version that exists.
define('BLOCKCYPHER_DEFAULT_API_VERSION', 'v1');

// Define the various coin / chain pairs.
define('BLOCKCYPHER_BLOCKCHAIN_BTCMAIN', 'btc.main');
define('BLOCKCYPHER_BLOCKCHAIN_BTCTEST3', 'btc.test3');
define('BLOCKCYPHER_BLOCKCHAIN_DOGEMAIN', 'doge.main');
define('BLOCKCYPHER_BLOCKCHAIN_LTCMAIN', 'ltc.main');
define('BLOCKCYPHER_BLOCKCHAIN_BCYTEST', 'bcy.test');

// SDK Logging.
define('BLOCKCYPHER_DEFAULT_LOG_FILEPATH', '/var/log/blockcypher.log');
define('BLOCKCYPHER_LOG_LEVEL_INFO', 'INFO');
define('BLOCKCYPHER_LOG_LEVEL_DEBUG', 'DEBUG');

// Trying to push a microtx larger than this will make the SDK throw an exception.
define('BLOCKCYPHER_MICROTX_MAX_AMOUNT', 4000000);

// Default webhook URLs.
define('BLOCKCYPHER_WEBHOOK_DEFAULT_URL', 'blockcypher/webhook/custom');
define('BLOCKCYPHER_WEBHOOK_DEFAULT_FORWARDING_ADDRESS_URL', 'blockcypher/webhook/payment-forward');

/**
 * Implements hook_permission().
 */
function blockcypher_permission() {
  return array(
    'administer blockcypher' => array(
      'title' => t('Administer BlockCypher settings'),
      'description' => t('Grants users the full administrative permissions for
        configuring the BlockCypher API Integration.'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_menu().
 */
function blockcypher_menu() {
  return array(
    // Administrative settings.
    'admin/config/services/blockcypher' => array(
      'title' => 'BlockCypher API configuration',
      'description' => 'Configure the BlockCypher API settings.',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('blockcypher_settings'),
      'access arguments' => array('administer blockcypher'),
      'file' => 'blockcypher.admin.inc',
    ),
    // Webhook endpoint URLs. Payment forwarding gets its own URL because
    // the payment forwarding payloads don't actually specify what kind
    // of event they are.
    BLOCKCYPHER_WEBHOOK_DEFAULT_URL => array(
      'page callback' => 'blockcypher_webhook_endpoint',
      // Open access because BlockCypher will hit this url as an unauthenticated
      // user whenever one of our custom webhooks fire.
      'access callback' => TRUE,
      'type' => MENU_CALLBACK,
      'file' => 'blockcypher.pages.inc',
    ),
    BLOCKCYPHER_WEBHOOK_DEFAULT_FORWARDING_ADDRESS_URL => array(
      'page callback' => 'blockcypher_webhook_forwarding_address_endpoint',
      // Open access because BlockCypher will hit this url as an unauthenticated
      // user whenever one of our payment-forwarding webhooks fire.
      'access callback' => TRUE,
      'type' => MENU_CALLBACK,
      'file' => 'blockcypher.pages.inc',
    ),
  );
}

/**
 * Implements hook_libraries_info().
 */
function blockcypher_libraries_info() {
  // This lets Libraries API search for 'sites/all/libraries/blockcypher'
  // directory, which should contain the entire, original extracted library.
  $libraries['blockcypher'] = array(
    // Only used in administrative UI of Libraries API.
    'name' => 'BlockCypher PHP SDK',
    'vendor url' => 'http://www.blockcypher.com',
    'download url' => 'https://github.com/blockcypher/php-client/releases',
    // Optional: If, after extraction, the actual library files are contained in
    // 'sites/all/libraries/example/lib', specify the relative path here.
    'path' => 'php-client',
    // Detect the installed version of the SDK.
    'version arguments' => array(
      'file' => 'php-client/blockcypher/php-client/release_notes.md',
      'pattern' => '/v(\d+\.\d+\.\d+)/',
    ),
    // Default list of files of the library to load.
    'files' => array(
      'php' => array(
        'autoload.php',
      ),
    ),
  );

  return $libraries;
}

/**
 * Initializes the connection and returns an ApiContext object.
 *
 * This is required before making any of the API calls.
 *
 * @param array $params
 *   An optional array of parameters to override the default API connection
 *   settings. Valid options are:
 *   - token: Use a different API token than the default.
 *   - api_version: Which API version to use.
 *   - coin: The cryptocoin we are working with: (btc|ltc|doge|bcy)
 *   - chain: The blockchain of the specified coin we want to connect to.
 *       (main|test|test3)
 *   - log_enabled: If set to TRUE, then the BlockCypher SDK will log its own
 *       messages to a file.
 *   - log_filename: If logging is enabled, this is the path to the file that
 *       will be written to.
 *   - log_level: The level of logging. Valid options are 'INFO' and 'DEBUG'.
 *
 * @return \BlockCypher\Rest\ApiContext
 *   A BlockCypher ApiContext object if all went well, or NULL if there was
 *   some sort of failure.
 */
function blockcypher_initialize($params = array()) {
  _blockcypher_load_library();
  module_load_include('inc', 'blockcypher', 'blockcypher.api');
  return blockcypher_create_api_context($params);
}

/**
 * Load the BlockCypher SDK library, so we can call functions from it.
 *
 * @todo: There's probably a better way to do this leveraging the libraries
 *   module, but I haven't figured it out.
 */
function _blockcypher_load_library() {
  // Make sure to only load it once per request.
  static $loaded = FALSE;

  if (!$loaded) {
    $library = libraries_load('blockcypher');
    $path = $library['library path'] . '/' . $library['path'] . '/' . key($library['files']['php']);
    require_once $path;
    $loaded = TRUE;
  }
}
