<?php
/**
 * @file
 * Administrative page callbacks for the BlockCypher module.
 */

/**
 * Form builder function for module settings.
 */
function blockcypher_settings() {
  $form['bc'] = array(
    '#type' => 'fieldset',
    '#title' => t('BlockCypher API Configuration'),
  );

  $blockcypher_link_args = array('attributes' => array('target' => '_blank'));
  $blockcypher_link = l(t('registering an account at BlockCypher'), 'http://www.blockcypher.com', $blockcypher_link_args);
  $api_token_args = array('!link' => $blockcypher_link);
  $api_token_description = t('A BlockCypher API token. You can acquire a token by !link', $api_token_args);
  $form['bc']['blockcypher_api_token'] = array(
    '#type' => 'textfield',
    '#title' => t('API Token'),
    '#default_value' => variable_get('blockcypher_api_token', ''),
    '#size' => 40,
    '#required' => TRUE,
    '#description' => $api_token_description,
  );

  $webhook_secret_description = t('A randomized string used to verify that requests to your webhook endpoint actually came from BlockCypher.  You do not need to change this unless your key has been compromised.  WARNING: changing this will cause requests from existing webhooks to fail!');
  $form['bc']['blockcypher_webhook_secret_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Webhook - Secret Key'),
    '#default_value' => variable_get('blockcypher_webhook_secret_key', ''),
    '#size' => 40,
    '#required' => TRUE,
    '#description' => $webhook_secret_description,
  );

  $form['bc']['blockcypher_default_blockchain'] = array(
    '#type' => 'radios',
    '#title' => t('Default Blockchain'),
    '#default_value' => variable_get('blockcypher_default_blockchain', BLOCKCYPHER_BLOCKCHAIN_BCYTEST),
    '#options' => array(
      BLOCKCYPHER_BLOCKCHAIN_BTCMAIN => t('Bitcoin / Main'),
      BLOCKCYPHER_BLOCKCHAIN_BTCTEST3 => t('Bitcoin / Testnet3'),
      BLOCKCYPHER_BLOCKCHAIN_DOGEMAIN => t('Dogecoin / Main'),
      BLOCKCYPHER_BLOCKCHAIN_LTCMAIN => t('Litecoin / Main'),
      BLOCKCYPHER_BLOCKCHAIN_BCYTEST => t('Blockcypher / Test'),
    ),
    '#required' => TRUE,
    '#description' => t('The default blockchain to be used in API calls.'),
  );

  $form['bc']['blockcypher_default_api_version'] = array(
    '#type' => 'radios',
    '#title' => t('API Version'),
    '#default_value' => variable_get('blockcypher_default_api_version', BLOCKCYPHER_DEFAULT_API_VERSION),
    '#options' => array(
      'v1' => t('Version 1'),
    ),
    '#required' => TRUE,
    '#description' => t('The version of the BlockCypher API service.'),
  );

  $form['logging'] = array(
    '#type' => 'fieldset',
    '#title' => t('Log Settings'),
  );
  $form['logging']['blockcypher_logging_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Logging Enabled'),
    '#default_value' => variable_get('blockcypher_logging_enabled', 0),
    '#description' => t('The BlockCypher SDK provides the option to log its own errors and info to a file. Click this box to enable it.'),
  );
  $form['logging']['blockcypher_log_level'] = array(
    '#type' => 'radios',
    '#title' => t('Log Level'),
    '#default_value' => variable_get('blockcypher_log_level', BLOCKCYPHER_LOG_LEVEL_INFO),
    '#options' => array(
      BLOCKCYPHER_LOG_LEVEL_INFO => t('Production'),
      BLOCKCYPHER_LOG_LEVEL_DEBUG => t('Development'),
    ),
    '#required' => TRUE,
    '#description' => t('The verbosity level of the API logging, if enabled.'),
  );
  $form['logging']['blockcypher_log_filepath'] = array(
    '#type' => 'textfield',
    '#title' => t('Blockcypher Logfile'),
    '#default_value' => variable_get('blockcypher_log_filepath', BLOCKCYPHER_DEFAULT_LOG_FILEPATH),
    '#description' => t('If logging is enabled, then it will be written to this file.'),
  );

  return system_settings_form($form);
}
