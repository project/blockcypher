CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers
 * API Documentation
   * Addresses
   * Blocks
   * Blockchains
   * Wallets
   * Transactions
   * Payment-Forwarding
   * Webhooks
 * Hooks Provided by this Module
 * Usage Examples



INTRODUCTION
------------

The BlockCypher module allows you to quickly and easily start querying and
transacting data on various blockchain networks (Bitcoin, Litecoin, Dogecoin,
Testnet) in your own Drupal projects by implementing the BlockCypher REST API
via their PHP SDK.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/figlesias/2718479

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2718479



REQUIREMENTS
------------

This module requires the following modules:

 * Libraries API (https://drupal.org/project/libraries)

Additionally, this module has the following php requirements:

  * PHP 5.4+

  * curl, json, openssl, gmp, and mcrypt extensions.



INSTALLATION
------------

 1 Download this module and extract it to the sites/all/modules folder. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 2 Download the latest BlockCypher PHP SDK release and extract it to the
   sites/all/libraries/blockcypher folder (make sure to download the client
   release, not the source).

   You can download the latest SDK here: https://github.com/blockcypher/php-client/releases

 3 Register for a free account on https://accounts.blockcypher.com
   to get an API key.

 4 Enable the module on your site, then configure it under
   Configuration > Web services > BlockCypher API configuration
   (admin/config/services/blockcypher).

 * It is strongly recommended that you enable HTTPS on your website, especially
   at the two webhook endpoint URLS (blockcypher/webhook/custom and
   blockcypher/webhook/payment-forward)



CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions:

   - Administer BlockCypher settings
     Users in roles with the "Administer BlockCypher settings" permission will
     have the ability to access this module's configuration page, where the API
     key and default blockchain settings can be set.

 * Configure the module settings under Configuration > Web services >
   BlockCypher API configuration (admin/config/services/blockcypher).
   At the very minimum, this module will not work until a valid API token is
   entered on this page.



TROUBLESHOOTING
---------------

 * If your API calls aren't working:

   - Visit the Status Report page under Reports > Status report and verify that
     the BlockCypher PHP SDK is installed correctly.

   - Enable logging under Configuration > Web services > BlockCypher API
     configuration. This will cause every call to the API to write debugging
     messages to a file, with HTTP status codes and error messages.



MAINTAINERS
-----------

Current maintainers:
 * Fernando Iglesias (fernando iglesias) - https://www.drupal.org/u/fernando-iglesias

The maintainer(s) of this module are not actually affiliated with anyone at http://www.blockcypher.com



API Documentation
-----------------

 * This documentation assumes some knowledge of bitcoin / blockchains in
   general.  For more details, see here: https://en.bitcoin.it/wiki/Main_Page

  Before making any calls to the API, we first need to initialize the SDK and
  create an APIContext object, which is done by calling the following function:

/**
 * Initializes the connection and returns an ApiContext object.
 *
 * This is required before making any of the API calls.
 *
 * @param array $params
 *   An optional array of parameters to override the default API connection
 *   settings. Valid options are:
 *   - token: Use a different API token than the default.
 *   - api_version: Which API version to use.
 *   - coin: The cryptocoin we are working with: (btc|ltc|doge|bcy)
 *   - chain: The blockchain of the specified coin we want to connect to.
 *       (main|test|test3)
 *   - log_enabled: If set to TRUE, then the BlockCypher SDK will log its own
 *       messages to a file.
 *   - log_filename: If logging is enabled, this is the path to the file that
 *       will be written to.
 *   - log_level: The level of logging. Valid options are 'INFO' and 'DEBUG'.
 *
 * @return \BlockCypher\Rest\ApiContext
 *   A BlockCypher ApiContext object if all went well, or NULL if there was
 *   some sort of failure.
 */
function blockcypher_initialize($params = array()) {...}


  To clarify, once you've done this:

    $api_context = blockcypher_initialize();

  You are ready to start calling all of the api functions provided by the module.
  Here is the complete list of API functions you can call:



ADDRESSES
---------

/**
 * For a given address, return an array of information about it.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $address
 *   An address on the blockchain.
 *
 * @return array
 *   An array containing all of the information about the requested address.
 */
function blockcypher_get_address($api_context, $address) {...}

/**
 * Generate a new address & private key.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 *
 * @return array
 *   An array containing all of the information about the newly-created address.
 */
function blockcypher_create_address($api_context) {...}

/**
 * Using multiple public keys, generate an n-of-m multisig address.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string[] $public_keys
 *   An array of public keys.
 * @param int $n
 *   The minimum number of signatures required to sign a transaction.
 *
 * @return array
 *   An array containing all of the information about the newly-created
 *   multisig address.
 */
function blockcypher_create_multisig_address($api_context, $public_keys, $n) {...}


BLOCKS
------

/**
 * Fetch information about a specific block.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param int|string $id
 *   If numeric, The block is fetched by height. If alphanumeric, the block is
 *   fetched by hash.
 *
 * @return array
 *   An array containing all of the information about the requested block.
 */
function blockcypher_get_block($api_context, $id) {...}

/**
 * Return an array of information about multiple requested blocks.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param int[]|string[] $id_array
 *   $ids An array of block ids (either the hash or the height).
 *
 * @return array
 *   An array of arrays (keyed by height), each sub-array containing information
 *   about a requested block.
 *
 * @see blockcypher_get_block()
 */
function blockcypher_get_blocks($api_context, $id_array) {...}


BLOCKCHAINS
-----------

/**
 * Return an array of top-level information about an entire blockchain.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 *
 * @return array
 *   An array containing information about the blockchain specified in
 *   the $api_context.
 */
function blockcypher_get_blockchain($api_context) {...}


WALLETS
-------

* It's worth pointing out that "Wallet" has nothing to do with private key
  management in Blockcypher.

* It's more like just a named collection of addresses.  For example, you can
  create a wallet for each username on your site and then easily pull up a
  given user's address list by name. You will still need to store the private
  keys on your own or they will be lost forever.

/**
 * Create a new (non-hd) wallet.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $name
 *   The name of the wallet. Note: these names are unique to the blockchain
 *   they're on. This means that you cannot have a wallet and an HD wallet with
 *   the same name on the same blockchain.
 * @param string[] $addresses
 *   An optional array of addresses to add to this wallet upon creation.
 *
 * @return array
 *   An array containing information on the newly-created wallet.
 */
function blockcypher_create_wallet($api_context, $name, $addresses = array()) {...}

/**
 * Get an existing (non-hd) wallet.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $name
 *   The name of the wallet.
 *
 * @return array
 *   An array containing information on the requested wallet.
 */
function blockcypher_get_wallet($api_context, $name) {...}

/**
 * Add one or more addresses to an existing wallet.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $name
 *   The name of the wallet.
 * @param string[] $addresses
 *   An array of addreses to add to the wallet.
 *
 * @return array
 *   An array with information about the newly-updated wallet.
 */
function blockcypher_add_addresses_to_wallet($api_context, $name, $addresses) {...}

/**
 * Remove one or more addresses from an existing wallet.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $name
 *   The name of the wallet.
 * @param string[] $addresses
 *   An array of addresses to remove from this wallet.
 *
 * @return bool
 *   TRUE if the removal succeeded, FALSE otherwise.
 */
function blockcypher_remove_addresses_from_wallet($api_context, $name, $addresses) {...}

/**
 * Delete an existing wallet.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $name
 *   The name of the wallet.
 *
 * @return bool
 *   TRUE if the wallet was successfully deleted, FALSE if there was an error.
 */
function blockcypher_delete_wallet($api_context, $name) {...}


TRANSACTIONS
------------

* DANGER: once a transaction is on the network it cannot be reversed!

/**
 * Fund an address from a faucet.
 *
 * If you try to do this on any blockchain other than BTC/Testnet3 or
 * BCY/Testnet then it will fail.
 *
 * On the BlockCypher Test Chain, the faucet will refuse to fund an address
 * with more than 500 billion BlockCypher satoshis and will not fund more than
 * 100 million BlockCypher satoshis at a time. On Bitcoin Testnet3 those numbers
 * are adjusted to 10 million and 500,000 testnet satoshis respectively.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $address
 *   The address to fund.
 * @param int $amount
 *   The amount (in satoshi) to send to this address.
 *
 * @return array
 *   An array containing the transaction id of the faucet funding the address.
 */
function blockcypher_fund_address_from_faucet($api_context, $address, $amount) {...}

/**
 * Fetch and return information for a given transaction on the blockchain.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $tx_hash
 *   The transaction hash.
 *
 * @return array
 *   An array containing information on the requested transaction.
 */
function blockcypher_get_transaction($api_context, $tx_hash) {...}

/**
 * Create a transaction, sign it, and then push it to the network.
 *
 * This function lets you send funds from one addresses to either a standard
 * or multisig address.
 *
 * If the transaction is small enough, it is sent as a blockcypher
 * microtransaction, which seems to save some fees.
 *
 * DANGER: Once a transaction is pushed to the network, it cannot be reversed!
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $src
 *   The address that you are sending from.
 * @param string|string[] $dest
 *   The address you are sending the coins to (or an array of public keys if
 *   the destination is a multisig address).
 * @param string $private
 *   The private key of the sending address, used to sign the transaction.
 * @param int $amount
 *   The amount (in satoshi) to send from the source address to the
 *   destination address.
 * @param string $dest_script_type
 *   If the receiving address is multisig, this needs to be a string in the
 *   format of 'multisig-n-of-m'.
 *
 * @todo: allow the user to specify a change address, instead of automatically
 *   refunding to the source address.
 * @todo: allow the user to spend from a multisig address
 *   (or do this in its own function?).
 * @todo: allow the user to specify the miner fee rather than letting the
 *   api decide on its own.
 *
 * @return array
 *   An array containing the information about the newly-created unconfirmed transaction.
 */
function blockcypher_create_and_send_transaction($api_context, $src, $dest, $private, $amount, $dest_script_type = 'pay-to-pubkey-hash') {...}

/**
 * Return the BlockCypher confidence rating for an unconfirmed transaction.
 *
 * This is defined as the probability that it will be included in the
 * next block. If this is called on a confirmed transaction, the API will return
 * an error.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $tx_hash
 *   The hash of the unconfirmed transaction.
 *
 * @return array
 *   An array with BlockCypher's confidence rating for the transaction,
 *   as a float from 0 to 1.
 */
function blockcypher_get_transaction_confidence($api_context, $tx_hash) {...}


PAYMENT FORWARDING
------------------

* Want to be able to give each user an address and then immediately sweep
  deposits to your hot-wallet / cold storage? Want to have a webhook
  automatically created so that your site gets pinged automatically with the
  relevant information when a payment is made? Then these functions are the
  greatest thing ever.

/**
 * Create a new forwarding address.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $dest_address
 *   The destination address. When funds are sent to this forwarding address,
 *   they are immediately forwarded to here.
 * @param string $callback_url
 *   This is the url that will get hit with the payload whenever funds are
 *   automatically forwarded. If not provided, it will use the default
 *   payment-forwarded webhook url provided by this module. You can then later
 *   react to this webhook by implementing hook_blockcypher_payment_forwarded().
 * @param array $callback_url_params
 *   An array of params to be combined with the $callback_url using Drupal's
 *   url() function.
 *
 * @return array
 *   An array containing all of the information on the newly-created
 *   forwarding address.
 */
function blockcypher_create_forwarding_address($api_context, $dest_address, $callback_url = NULL, $callback_url_params = array()) {...}

/**
 * Return the information about a specific forwarding address for a given ID.
 *
 * This ID isn't an address - it's the 'id' component assigned automatically
 * when you created the forwarding address.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $id
 *   The ID of the forwarding address.
 *
 * @return array
 *   An array of information about the existing forwarding address.
 */
function blockcypher_get_forwarding_address($api_context, $id) {...}

/**
 * Fetch the complete list of forwarding addresses created for your API token.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 *
 * @return array
 *   An array of arrays, each describing an existing forwarding address.
 */
function blockcypher_get_forwarding_address_list($api_context) {...}

/**
 * Delete a payment forwarding address by its given ID.
 *
 * This ID isn't an address - it's the 'id' component assigned automatically
 * when you created the forwarding address.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $id
 *   The ID of the forwarding address.
 *
 * @return bool
 *   Return TRUE if deleted successfully, FALSE if there was an error.
 */
function blockcypher_delete_forwarding_address($api_context, $id) {...}


WEBHOOKS
--------

/**
 * Webhooks.
 * The BlockCypher API allows you setup webhooks such that anytime a specified
 * event that you want to be notified about occurs (like your transaction was
 * confirmed, a new block was mined, a double-spend was detected, etc) then
 * BlockCypher will deliver a JSON payload to the site detailing it. You can
 * then react to this in your own module by implementing
 * hook_blockcypher_webhook_event().
 */

/**
 * Create a new Webhook.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $event_type
 *   This determines what will cause this webhook to trigger. Valid options are:
 *   - unconfirmed-tx: Triggered for every new transaction BlockCypher receives
 *       before it’s confirmed in a block; basically, for every unconfirmed
 *       transaction. The payload is an unconfirmed TX.  Warning:
 *       if unconstrained, this webhook will spam your $callback_url
 *   - new-block: Triggered for every new block added to the blockchain you’ve
 *       selected as your base resource. The payload is a Block.
 *   - confirmed-tx: Triggered for every new transaction making it into a new
 *       block; in other words, for every first transaction confirmation. This
 *       is equivalent to listening to the new-block event and fetching each
 *       transaction in the new Block. The payload is a confirmed TX.
 *   - tx-confirmation: Simplifies listening to confirmations on all
 *       transactions for a given address up to a provided threshold. Sends
 *       first the unconfirmed transaction and then the transaction for each
 *       confirmation. Use the confirmations property within the Event to
 *       manually specify the number of confirmations desired (maximum 10,
 *       defaults to 6). The payload is a TX.
 *   - double-spend-tx: Triggered any time a double spend is detected by
 *       BlockCypher. The payload is the TX that triggered the event; the hash
 *       of the transaction that it’s trying to double spend is included in its
 *       double_spend_tx property.
 *   - tx-confidence: Triggered any time an address has an unconfirmed
 *       transaction above the confidence property specified in the Event, based
 *       on our Confidence Factor. The payload is the TX that triggered the
 *       event. If confidence is not set, defaults to 0.99. To ensure
 *       transactions are not missed, even if your confidence threshold is not
 *       reached, a transaction is still sent after a minute timeout; please
 *       remember to double-check the confidence attribute in the TX payload.
 * @param string $hash
 *   Only objects matching this hash will be sent. The hash can be either for
 *   a block or a transaction.
 * @param string $address
 *   Only transactions associated with this address will be sent. This can
 *   either be an address OR the name of a wallet!
 * @param string $callback_url
 *   This is the url that will get hit with the payload when the webhook fires.
 *   If not provided, it will use the default webhook url provided by this
 *   module. You can then later react to this webhook by implementing
 *   hook_blockcypher_webhook_event().
 * @param array $callback_url_params
 *   An array of params to be combined with the $callback_url using
 *   Drupal's url() function.
 *
 * @return array
 *   An array describing the newly-created webhook.
 */
function blockcypher_create_webhook($api_context, $event_type, $hash = NULL, $address = NULL, $callback_url = NULL, $callback_url_params = array()) {...}

/**
 * Return all of the info on an existing webhook, by id.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $id
 *   The id of the webhook.
 *
 * @return array
 *   An array describing the existing webhook.
 */
function blockcypher_get_webhook($api_context, $id) {...}

/**
 * Return an array of all webhooks created for this api token.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 *
 * @return array
 *   An array of arrays, each containing information on an existing webhook.
 */
function blockcypher_get_webhook_list($api_context) {...}

/**
 * Delete an existing webhook, by id.
 *
 * @param object $api_context
 *   An API context object created by blockcypher_create_api_context().
 * @param string $id
 *   The ID of the existing webhook.
 *
 * @return bool
 *   TRUE if the webhook was deleted successfully, FALSE if there was an error.
 */
function blockcypher_delete_webhook($api_context, $id) {...}




HOOKS PROVIDED BY THIS MODULE
-----------------------------

 * This module provides two hooks for you to implement in your own modules.
   Both of them let you react to webhook events sent by the blockcypher API.

   function hook_blockcypher_webhook_payment_forwarded($data) {...}

   * This hook is invoked whenever one of your forwarding addresses has received
     funds and successfully forwarded them to their destination addresses.  The
     $data variable is an array containing information about the address and the
     hash of the transaction that forwarded the payment.

   function hook_blockcypher_webhook_event($data) {...}

   * This hook is invoked whenever one of your custom webhooks is triggered. The
     $data variable is an array containing information about the webhook that
     was triggered (it will contain the event type, and potentially
     various hashes, addresses, etc.)



USAGE EXAMPLES
--------------

 * Create two new addresses, fund one with 10 mBTC from a faucet and then send
   5 mBTC to the other address.

   $api = blockcypher_initialize();

   $sender = blockcypher_create_address($api);
   blockcypher_fund_address_from_faucet($api, $sender['address'], $amount = 1000000);

   $receiver = blockcypher_create_address($api);

   $transaction = blockcypher_create_and_send_transaction(
     $api,
     $sender['address'],
     $receiver['address'],
     $sender['private'],
     $amount = 500000
   );
